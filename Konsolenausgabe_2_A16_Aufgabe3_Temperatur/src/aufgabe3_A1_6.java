
public class aufgabe3_A1_6 {

	public static void main(String[] args) {
		
		//aufgabe 3 [A1.6]
		int a = 20;
		int b = 10;
		int c = 0;
		int d = 30;
		
		double e = 28.8889;
		double f = 23.3333;
		double g = 17.7778;
		double h = 6.6667;
		double i = 1.1111;
		
		/*integer variable declarations are necessary for %d but not for string (%s)*/
		
		System.out.printf("%-12s|%10s%n", "Fahrenheit", "Celsius");
		System.out.printf("%-24s%n", "------------------------");
		System.out.printf("%-12d|%10.2f%n", -a, -e);
		System.out.printf("%-12d|%10.2f%n", -b, -f);
		System.out.printf("%+-12d|%10.2f%n", c, -g);
		System.out.printf("%+-12d|%10.2f%n", a, -h);
		System.out.printf("%+-12d|%10.2f%n", d, -i); //%n could be ignored here, cause it's the last function
		
	}

}
