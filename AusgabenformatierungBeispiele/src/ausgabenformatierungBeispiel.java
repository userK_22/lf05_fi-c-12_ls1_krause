
public class ausgabenformatierungBeispiel {

	public static void main(String[] args) {
		
		/*
		//	Ganze Zahl
		int zahl = 123456789;
		System.out.println("#### Ganze Zahlen #### \n");
		System.out.printf("|%d| \n", zahl); 				// | --> ist das Pipe-Symbol (Linux)
		System.out.printf("|%20d| \n", zahl); 				// 20 zeichen (Standard = rechtsbuendig)
		System.out.printf("|%-20d| \n", zahl);				// Minus nach % = linksbuendig
		System.out.println("\n");
		
		//	Kommazahlen
		System.out.println("#### Kommazahlen #### \n"); 	// \n ist neuer Absatz, %n ist neu Zeile
		System.out.printf("|%f| \n", 12345.123456789);
		System.out.printf("|%.2f| \n", 12345.123456789);
		System.out.printf("|%20.2f| \n", 12345.123456789);
		System.out.printf("|%-20.2f| \n", 12345.123456789);
		System.out.println(" \n");
		
		// Zeichenketten
		System.out.println("#### Zeichenketten #### \n");
		System.out.printf("|%s| \n", 12345.123456789);
		System.out.printf("|%20s| \n", 12345.123456789);
		System.out.printf("|%-20s| \n", 12345.123456789);
		System.out.printf("|%-20.4s| \n", 12345.123456789);
		System.out.println("\n");
		*/
		
		//Aufgabe
		System.out.printf("Name: %-10s,Alter: %-10d,Gehalt: %-10.2f Euro", "Max", 18, 1801.50);
		

	}

}
