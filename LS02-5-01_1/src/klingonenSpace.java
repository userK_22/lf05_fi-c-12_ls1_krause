
public class klingonenSpace {
	
	private int photonenTorpedoAnzahl;
	private int energieVersorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungsSystemeInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	public klingonenSpace() {
		System.out.println("Constuctor " + getSchiffsname() + " loaded!");
	}

	public klingonenSpace(int photonenTorpedoAnzahl, int energieVersorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungsSystemeInProzent, String schiffsname, int androidenAnzahl) {
		
		System.out.println("Klingonen_Objekt mit Params");
		
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
		this.energieVersorgungInProzent = energieVersorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungsSystemeInProzent = lebenserhaltungsSystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
		//this.ladungsverzeichnis = ladungsverzeichnis;

	}

	public int getPhotonenTorpedoAnzahl() {
		return photonenTorpedoAnzahl;
	}

	public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
	}

	public int getEnergieVersorgungInProzent() {
		return energieVersorgungInProzent;
	}

	public void setEnergieVersorgungInProzent(int energieVersorgungInProzent) {
		this.energieVersorgungInProzent = energieVersorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungsSystemeInProzent() {
		return lebenserhaltungsSystemeInProzent;
	}

	public void setLebenserhaltungsSystemeInProzent(int lebenserhaltungsSystemeInProzent) {
		this.lebenserhaltungsSystemeInProzent = lebenserhaltungsSystemeInProzent;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	

	// Methoden

	public void photonentorpedoSchiessen(klingonenSpace k) {
		System.out.println("Feuer");
	} // schiessen

	public void phaserkanoneSchiessen(klingonenSpace k) {
		System.out.println("Feuer Phaser");
	} // schiessen Phaser

	private void treffer(klingonenSpace k) {
		System.out.println("Wir wurden getroffen");
	} // Treffer Anzahl

	public void zustandRaumschiff(klingonenSpace k) {
		System.out.println("Schiff beschädigt");
	} // Zustand Schiff

	public void nachrichtAnAlle(String message) {
		System.out.println("Nachricht an Alle");
	} // Nachricht
	
	public void addLadung(ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
		
	}
	
}

