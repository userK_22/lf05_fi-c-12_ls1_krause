import java.util.Scanner;

public class mainSpaceShuttle {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		klingonenSpace k = new klingonenSpace(1, 100, 100, 100, 100, "IKS Heghta", 2);
		romulanerSpace r = new romulanerSpace(2, 100, 100, 100, 100, "IRW Khazara", 2);
		vulkanierSpace v = new vulkanierSpace(0, 80, 80, 50, 100, "NiVar", 5);
		
		ladung k1 = new ladung("Ferengi Schneckensaft", 200);
		ladung k2 = new ladung("Bat'leth Klingonen Schwert", 200);
		
		ladung r1 = new ladung("Borg-Schrott", 5);
		ladung r2 = new ladung("Rote Materie", 2);
		ladung r3 = new ladung("Plasma-Waffe", 50);
		
		ladung v1 = new ladung("Forschongssonde", 35);
		ladung v2 = new ladung("Photonentorpedo", 3);
		
		k.setPhotonenTorpedoAnzahl(1); //Testwert = 300
		k.setEnergieVersorgungInProzent(100);
		k.setSchildeInProzent(100);
		k.setHuelleInProzent(100);
		k.setLebenserhaltungsSystemeInProzent(100);
		k.setSchiffsname("IKS Heghta");
		k.setAndroidenAnzahl(2);
		
		r.setPhotonenTorpedoAnzahl(2);
		r.setEnergieVersorgungInProzent(100);
		r.setSchildeInProzent(100);
		r.setHuelleInProzent(100);
		r.setLebenserhaltungsSystemeInProzent(100);
		r.setSchiffsname("IRW Khazara");
		r.setAndroidenAnzahl(2);
		
		v.setPhotonenTorpedoAnzahl(0); //Testwert = 300
		v.setEnergieVersorgungInProzent(80);
		v.setSchildeInProzent(80);
		v.setHuelleInProzent(50);
		v.setLebenserhaltungsSystemeInProzent(100);
		v.setSchiffsname("NiVar");
		v.setAndroidenAnzahl(5);
		
		k1.setBezeichnung("Ferengi Schneckensaft");
		k1.setMenge(200);
		k2.setBezeichnung("Bat'leth Klingonen Schwert");
		k2.setMenge(200);
		
		r1.setBezeichnung("Borg-Schrott");
		r1.setMenge(5);
		r2.setBezeichnung("Rote Materie");
		r2.setMenge(2);
		r3.setBezeichnung("Plasma-Waffe");
		r3.setMenge(50);
		
		v1.setBezeichnung("Forschongssonde");
		v1.setMenge(35);
		v2.setBezeichnung("Photonentorpedo");
		v2.setMenge(3);
		
		//add Ladung
		k.addLadung(k1);
		k.addLadung(k2);
		
		r.addLadung(r1);
		r.addLadung(r2);
		r.addLadung(r3);
		
		v.addLadung(v1);
		v.addLadung(v2);
		
	}
	
}
