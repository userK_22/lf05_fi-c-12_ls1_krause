
public class Auto {
	
	public String marke; //private
	public String farbe; //private
	
	/*private String besitzer;
	private int hubraum;
	private int baujahr;*/
	
	public Auto() {
		System.out.println("Auto_Objekt ohne Params");
	}
	
	public Auto(String marke, String farbe) {
		
		System.out.println("Auto_Objekt mit Params");
		
		this.marke = marke;
		this.farbe = farbe;

	}
	
	public String getMarke() {
		return this.marke;
	}
	
	public void setMarke(String marke) {
		this.marke = marke;
	}
	
	
	public String getFarbe() {
		return this.farbe;
	}
	
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	//weitere Methoden
	public void fahre(int Strecke) {
		System.out.println("Strecken-Parameter");
	}
	
	public void tanken(int liter) {
		System.out.println("[Super Plus] ...das wird teuer!");
	}

}
