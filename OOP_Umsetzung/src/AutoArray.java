import java.util.ArrayList;

public class AutoArray {

	private ArrayList<LadungAryAuto> ladungen;
	private String marke;
	private String farbe;
	
	public AutoArray() {
		
	}

	
	public AutoArray(String marke, String farbe) {
		this.marke = marke;
		this.farbe = farbe;
	}
	
	public String getMarke() {
		return this.marke;
	}
	
	public void setMarke(String marke) {
		this.marke = marke;
	}
	
	public String getFarbe() {
		return this.farbe;
	}
	
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

}
