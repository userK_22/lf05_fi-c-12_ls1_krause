package for_schleife_AB;

import java.util.Scanner;

public class aufgabe1 {

	public static void main(String[] args) {
		
		Scanner firstNum = new Scanner(System.in);
		int n = firstNum.nextInt();
		firstNum.close();
		
		for (int i = n; i >= 1; i--)
		{
			System.out.println(i);
		}

	}
}

