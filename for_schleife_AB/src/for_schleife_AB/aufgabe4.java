package for_schleife_AB;

public class aufgabe4 {

	public static void main(String[] args) {
		
	/* a */
		
		for (int i = 99; i >= 9; i = i - 3)
		{
			System.out.println(i);
		}
	
	/* c */
		
		for (int n = 2; n <= 102; n = n + 4)
		{
			System.out.println(n);
		}
	
	/* d */
		
		int z = 12;
		
		for (int x = 4; x <= 1024; x = x + z)
		{
			System.out.println(x);
			z = z + 8;
		}
	}
}

