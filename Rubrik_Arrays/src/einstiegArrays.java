
public class einstiegArrays {

	public static void main(String[] args) {
		
		int[] array = new int[5]; //--> Felder als Array erstellen (int)
		
		System.out.println(array.length);
		
		for (int i = 0; i <= array.length -1; i++)
		{
			array[i] = 1; //--> allen Feldern im Array den Wert 1 zuweisen
		}
		
		/*
		for (int i = 0; i <= 4; i++)
		{
			array[i] = 1; //--> allen Feldern im Array den Wert 1 zuweisen
		}
		*/
		
		for (int j = 0; j <= 4; j++)
		{
			System.out.println(array[j]);
		}

	}

}
