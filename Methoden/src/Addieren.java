import java.util.Scanner;

public class Addieren {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein: ");
		int zahl01 = scn.nextInt();
		
		System.out.println("Geben Sie eine weitere Zahl ein: ");
		int zahl02 = scn.nextInt();
		
		int autoSum = zahl01 + zahl02;
		
		System.out.println(zahl01 + " + " + zahl02 + " = " + autoSum);
		
		
		// Einfuehrung in Methoden
		
		System.out.println(" ###### BREAK ######");
		
//		int zahl012 = getUserInput();
//		int zahl021 = getUserInput();
		int summe = addiere(zahl01, zahl02); //addiere(zahl012, zahl021)
		gebeSummeAus(summe);
		
		scn.close();
		
	}
	public static void gebeSummeAus(int zahl) {
		System.out.println("Summe: " + zahl);

	}
	public static int addiere(int zahl_01, int zahl_02) {
		int summe = zahl_01 + zahl_02;
		return summe;
	}
	public static void getUserInput(int inpZahl) {
		Scanner scn1 = new Scanner(System.in);
		inpZahl = scn1.nextInt();
		scn1.close(); //maybe right
	}

}
