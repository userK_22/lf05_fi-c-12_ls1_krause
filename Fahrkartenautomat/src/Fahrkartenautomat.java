﻿import java.util.ArrayList;
import java.util.Scanner;

public class Fahrkartenautomat
{
    public static void main(String[] args) {
	
    	while (true) {
			
		int usrAuswahl;
		//Scanner tastatur = new Scanner(System.in);
		
		//Neues Array
		ArrayList<Integer> fahrkarten = new ArrayList<>();
		       
		//double zuZahlenderBetrag;					//
		//double eingezahlterGesamtbetrag;			//
		//double eingeworfeneMünze;					//
		//double rückgabebetrag; 					//
		       
		double zuZahlenderBetrag = 0.0;

		do {
           usrAuswahl = fahrkartenbestellungErfassung();
        	   
           if ((usrAuswahl > 10 || usrAuswahl < 0) && usrAuswahl != 0) {
        	   System.out.println("Sie haben " + usrAuswahl + " gewählt.");
        	   System.out.println(">>falsche Eingabe<<");
        	   } else if (usrAuswahl != 0) {
        		   zuZahlenderBetrag = zuZahlenderBetrag + fahrkartenbestellungErfassen(usrAuswahl);
        	   }

           System.out.println("\n");

           } while (usrAuswahl != 9);

		
			double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			geldAusgabe(rückgabebetrag);

			System.out.println("\n");

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }}
    
    //Auswahloptionen
	public static int fahrkartenbestellungErfassung() {
		Scanner tastatur = new Scanner(System.in);
		int usrAuswahl;

		System.out.println("\n");
		System.out.println("Wählen Sie ein Ticket für das Tarifgebiet Berlin AB:");
		System.out.println("\n");
		System.out.println("Vorgangsnummer	  Ticketbezeichnung				    Preis(EUR)");
		System.out.println("--------------------------------------------------------------");
		System.out.println("1.................Einzelfahrschein Berlin AB..............2,90");
		System.out.println("2.................Einzelfahrschein Berlin BC..............3,30");
		System.out.println("3.................Einzelfahrschein Berlin ABC.............3,60");
		System.out.println("4.................Kurzstrecke.............................1,90");
		System.out.println("5.................Tageskarte Berlin AB....................8,60");
		System.out.println("6.................Tageskarte Berlin BC....................9,00");
		System.out.println("7.................Tageskarte Berlin ABC...................9,60");
		System.out.println("8.................Kleingruppen-Tageskarte Berlin AB......23,50");
		System.out.println("9.................Kleingruppen-Tageskarte Berlin BC......24,30");
		System.out.println("10................Kleingruppen-Tageskarte Berlin ABC.....24,90");
		System.out.println("0.................>>Zahlen<<..................................");
		System.out.println("\n");
		System.out.printf("Wählen Sie eine Nummer der Vorgänge: ");

		usrAuswahl = tastatur.nextInt();

		return usrAuswahl;
	}
	
	//Auflistung der Ticketarten mit Switsh-Case-Methode
	public static double fahrkartenbestellungErfassen(int fahrkartenart) {
		Scanner tastatur = new Scanner(System.in);

		double anzahlTickets;

		double ticketEinzelpreis = 0;
		
	    switch (fahrkartenart)
	    {

	    	case 1:
				System.out.println("Preis pro Tickes: 2,90€ ");
				ticketEinzelpreis = 2.90;
	    		break;
	    	case 2:
				System.out.println("Preis pro Ticket: 3,30€ ");
				ticketEinzelpreis = 3.30;
	    		break;
	    	case 3:
				System.out.println("Preis pro Ticket: 3,60€ ");
				ticketEinzelpreis = 3.60;
	    		break;
	    	case 4:
				System.out.println("Preis pro Ticket: 1,90€ ");
				ticketEinzelpreis = 1.90;
	    		break;
	    	case 5:
				System.out.println("Preis pro Ticket: 8,60€ ");
				ticketEinzelpreis = 8.60;
	    		break;
	    	case 6:
				System.out.println("Preis pro Ticket: 9,00€ ");
				ticketEinzelpreis = 9.00;
	    		break;
	    	case 7:
				System.out.println("Preis pro Ticket: 9,60€ ");
				ticketEinzelpreis = 9.60;
	    		break;
	    	case 8:
				System.out.println("Preis pro Ticket: 23,50€ ");
				ticketEinzelpreis = 23.50;
	    		break;
	    	case 9:
				System.out.println("Preis pro Ticket: 24,30€ ");
				ticketEinzelpreis = 24.30;
	    		break;
	    	case 10:
				System.out.println("Preis pro Ticket: 24,90€ ");
				ticketEinzelpreis = 24.90;
	    		break;
	    	/*default:
	    		System.out.println("An Error ocourred");
	    		break;*/
	    }

	//Fallunterscheidung Ticketanzahl	
		do {
			System.out.print("Wählen Sie wie viele Tickets erworben werden sollen? [Max. 10 Tickets]: ");
			anzahlTickets = tastatur.nextInt();

			if (anzahlTickets > 10 || anzahlTickets < 0) {

				System.out.println("Sie können nur min. ein, oder max. 10 Tickets erwerben!");
			}

		} while (anzahlTickets > 10 || anzahlTickets < 0);

		return ticketEinzelpreis * anzahlTickets;

	}
	
	//Fahrkarte erwerben
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
			System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					"  ");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
    
	//Rückgeldberechnung und -ausgabe
    public static void geldAusgabe(double rückgabebetrag)
    {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
    
    //Fahrscheinausgabe
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben.");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	//tastatur.close();
}


