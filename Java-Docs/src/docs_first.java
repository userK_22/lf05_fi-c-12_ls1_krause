
/**
 * 
 * @author KEKRAUSE
 * @since 18.05.2022
 * @version 1.0
 * 
 */

public class docs_first {

	private String vorname;
	private String nachname;
	private int alter;
	
	public docs_first() {
		
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	/**
	 * 
	 * @return
	 */

	public String getNachname() {
		return nachname;
	}
	
	/**
	 * 
	 * @param nachname
	 */

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	/**
	 * 
	 * @return
	 */

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}
	
	/**
	 * 
	 * @param menge
	 */
	public void esse(int menge, String art) {
	}
	
	
	public String toString() {
		return "Vorname:    " + vorname +"\n" +
				"Nachname:  " + nachname +"\n" +
				"Alter:     " +alter +"\n";
	}

}
